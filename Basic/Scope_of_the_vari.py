# A variable is only available from inside the region it is created. This is called scope.

# Local Scope
# Global Scope

a = 10 # it is a global variable

def fun():
    b = 20 # local variable , it will not available for outside 
    print (b)


print(a)
fun();# function call
print (b) # error cant print outside 


# we can use global variables in side any function 
def fun2():
    b2 = 20 # local variable , it will not available for outside 
    print (b2)
    print(a) # we can call easley 


fun2()#

# you can access any globle variable defined bed=fore the function call

a4 = 13
def fun4():
    a4 = 14
    print (a4) # - > 14 
     # if u try to access global variable it is easy 
    # but if you write it like then python is assume thst 
    # a4 -> is a local and it is use as a local my my global variable will not be changed

print(a4) # 13 

# if you want to use global variable in side any function and try to change thair value the you shoud use
# a keyword  - > global


a4 = 13
def fun4():
    global a4 
    a4 = 14
    print (a4) # - > 14 
     

print(a4) # 14 # in this case my original value will be change  


   