# creating any list 
li = [] # it is an empty list and its name is li

# 2nd 
li2 = [1,2,3]
print(li2)

# heterogeneous list 
l = [1,2,30.45,"Mrinal" , 45.4]
print(l) # [1, 2, 30.45, 'Mrinal', 45.4]


# access and chage the elements of the list 

# so it is an index based 
# so we can access them using index

print(l[3])
print(l[2])
print(l[1])

# to update thair values we can use 
l[3] = " Singh "
print(l[3])
print(l) # [1, 2, 30.45, ' Singh ', 45.4]


# sliceing the list 
# i can do like i want a part of the list 
# or u can say that sub list 

# lets we want to access 1 to 2 index 
print(l[1:3]) # [2, 30.45] # starting from 1 and it will go till 2 bcz it end index is 3 so it will go till 2+1 endx it means 2 nd index 


print(l[1:]) # starting from 1th index and it will go till end 
# [2, 30.45, ' Singh ', 45.4]



# insert and appenfd element 
thislist1 = ["apple", "banana", "cherry"]
thislist1.insert(1, "orange")
print(thislist1)

print("----------------------------------------------")
thislist = ["apple", "banana", "cherry"]
thislist.append("orange")
print(thislist) # ['apple', 'banana', 'cherry', 'orange'] 

print("----------------------------------------------")

l5 = [5,3,4,'parteek' , 'Rohan' , 'Ankush', 10 , 'radha']

print(l5)
print("----------------------------------------------")
l5.append(9,10,11)

print(l5)

# // how thw element is stored in the list 


# it store referance of elements in sequ


#-------------------------------------------------------------------------
# taking input in list 
# first we shoud understand line seperates 
 p = input() 
 print(type(p)) # it will we by def str


 # so we have to convert it into the integer 


# from the user

# Take input of the size of the list
n = int (input ("Enter number of elements: ")) 

# Declare an empty list
list = []

# Iterate for n times take inputs
for i in range (n):
    # Take input of ith element as x.
    x = int(input())
    # Append 'x' to the list.
    list.append(x)

# Print the list
print("List:", list)


for ele in list:
    print(ele)



#  SpaceSeperatedInput 
# Using the split function to take the input. 
list = input("Enter elements: ").split()

# Printign the list.
print("List: ", list)

# output ->
# Enter elements: 1 2 3 4 5 A B C
# List: ['1', '2', '3', '4', '5', 'A', 'B', 'C']

#  but it is in string 



