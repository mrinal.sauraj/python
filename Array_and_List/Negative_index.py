# negative indexing

li  = [1,2,3,4,5,6 ,7,8,9,10]
 
print(li[0])
print(li[1])
print(li[2])
print(li[3])
print(li[4])

print("----------------")

print(li[-1])
print(li[-2])
print(li[-3])
print(li[-4])
print("------------------")

# print(li[-12]) # it will give you an error 

# sequqence of list -> it is same as range function 
# it can take 3 parameters 1) start 
# 2nd -> end  and 3rd step
# syntex 
# list [ start : end : step ] 

print(li [ 1:5:1])# [2,3,4,5]


# 2 step at one time 

print(li [ 1:5:2]) # [2,4]

print(li[1:])# it will start from 1st index and go till end and 
# its step will be + 1


# li  = [1,2,3,4,5,6 ,7,8,9,10]
print(li[1::1]) #[2, 3, 4, 5, 6, 7, 8, 9, 10]








