li = [1,2,3,4,"mrinal"]

# using range function
for i in range (len(li)):
      print(li[i])
print("-------------------------------")
# 
      
for ele in li:
      print(ele)

# if u want to slice of the list 
for ele in li[2:]:
      print(ele)

# print in some range 
for ele in li[2:4]:
      print(ele)
